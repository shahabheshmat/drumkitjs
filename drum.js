
const buttonAndAudioPair = {
};

document.querySelectorAll(".drum-button").forEach(button => {
    button.addEventListener("click", () => playSound(button.dataset.key));
});

document.addEventListener('keydown', (event) => {
    console.log(event);
    const key = event.key;
    const button = document.querySelector(`.drum-button[data-key=${key}]`);
    if (button) {
        button.dispatchEvent(new Event('click'));
    }
});

function playSound(key) {
    const audioElement = document.querySelector(`audio[data-key='${key}']`);
    if (audioElement) {
        audioElement.currentTime = 0;
        audioElement.play();
    }
}